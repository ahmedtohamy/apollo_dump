# qssi-user 12
12 SKQ1.211006.001 V13.0.7.0.SJDEUXM release-keys
- manufacturer: xiaomi
- platform: kona
- codename: apollo
- flavor: qssi-user
- release: 12
12
- id: SKQ1.211006.001
- incremental: V13.0.7.0.SJDEUXM
- tags: release-keys
- fingerprint: Redmi/apollo_eea/apollo:12/RKQ1.211001.001/V13.0.7.0.SJDEUXM:user/release-keys
- is_ab: false
- brand: Redmi
- branch: qssi-user-12
12-SKQ1.211006.001-V13.0.7.0.SJDEUXM-release-keys
- repo: redmi_apollo_dump
